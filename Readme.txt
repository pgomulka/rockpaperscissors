TDD kata
source:
http://stackoverflow.com/questions/15300521/can-anybody-point-me-in-the-right-direction-with-a-bdd-atdd-and-tdd-kata


 Rock, Paper, Scissors

 

User Story Front

+--------------------------------------------------+

|                                                  |

|     Title: Waste an Hour Having Fun              |

|                                                  |

| As a frequent games player,                      |

| I'd like to play rock, paper, scissors           |

| so that I can spend an hour of my day having fun |

|                                                  |

| Acceptance Criteria                              |

|  - Can I play Player vs Computer?                |

|  - Can I play Computer vs Computer?              |

|  - Can I play a different game each time?        |

|                                                  |

|                                                  |

+--------------------------------------------------+

 

User Story Back

+--------------------------------------------------+

|                                                  |

|                                                  |

|                                                  |

| Technical Constraints                            |

|                                                  |

| - Doesn't necessarily need a flashy GUI          |

|   (can be simple)                                |

| - Use Java                                       |

| - Libs / external modules should only be used    |

|   for tests                                      |

| - Using best in industry agile engineering       |

|   practices                                      |

|                                                  |

|                                                  |

|                                                  |

+--------------------------------------------------+

 

Don't know the game? http://en.wikipedia.org/wiki/Rock-paper-scissors