package com.gomul.rockpaperscissors;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.BDDMockito.given;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.gomul.rockpaperscissors.game.Game;

@RunWith(MockitoJUnitRunner.class)
public class GameModeParserTest {
	@InjectMocks
	GameModeParser parser = new GameModeParser();
	@Mock
	UserConsole userConsole;

	@Test
	public void shouldParseComputerVsComputerMode() {
		// given
		given(userConsole.readInput()).willReturn("cvc");
		
		// when
		Game game = parser.parse(userConsole);
		
		// then
		assertThat(game).isNotNull();
	}
	
	@Test
	public void shouldParsePlayerVsComputerMode() {
		// given
		given(userConsole.readInput()).willReturn("pvc");
		
		// when
		Game game = parser.parse(userConsole);
		
		// then
		assertThat(game).isNotNull();
	}

	@Test
	public void shouldThrowExceptionWhenUnrecognizableCommand() {
		// given
		given(userConsole.readInput()).willReturn("STHWRONG");
		
		try {
			// when
			Game game = parser.parse(userConsole);
			fail("should fail");
		} catch (IllegalArgumentException e) {
			// then
		}
	}
}
