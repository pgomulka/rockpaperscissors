package com.gomul.rockpaperscissors.game;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.gomul.rockpaperscissors.UserConsole;
import com.gomul.rockpaperscissors.game.shape.Shape;

@RunWith(MockitoJUnitRunner.class)
public class GameTest {
	@Mock
	RealPlayer player1;
	@Mock
	Computer computer;
	@Mock
	UserConsole userConsole;

	Game game;

	@Before
	public void init() {
		game = new Game(player1, computer, userConsole);
	}

	public void shouldBeAblePlayComputerVsComputer() {
		// given
		Computer computer2 = mock(Computer.class);
		Game game = new Game(computer, computer2, userConsole);

		given(computer.getShape()).willReturn(Shape.Paper, Shape.Paper, Shape.Paper);
		given(computer2.getShape()).willReturn(Shape.Scissors, Shape.Scissors, Shape.Scissors);

		// when
		game.play();

		// then
		verify(userConsole).print("0 3");
	}

	@Test
	public void computerShouldWinAll() {
		// given
		given(computer.getShape()).willReturn(Shape.Scissors, Shape.Scissors, Shape.Scissors);
		given(player1.getShape()).willReturn(Shape.Paper, Shape.Paper, Shape.Paper);

		// when
		game.play();

		// then
		verify(userConsole).print("0 3");
	}

	@Test
	public void computerShouldWinTwoOfThree() {
		// given
		given(computer.getShape()).willReturn(Shape.Scissors, Shape.Scissors, Shape.Scissors);
		given(player1.getShape()).willReturn(Shape.Stone, Shape.Paper, Shape.Paper);

		// when
		game.play();

		// then
		verify(userConsole).print("1 2");
	}

	@Test
	public void thereShouldBeATie() {
		// given
		given(computer.getShape()).willReturn(Shape.Scissors, Shape.Scissors, Shape.Scissors);
		given(player1.getShape()).willReturn(Shape.Scissors, Shape.Scissors, Shape.Scissors);

		// when
		game.play();

		// then
		verify(userConsole).print("0 0");
	}
}
