package com.gomul.rockpaperscissors.game;

import static org.fest.assertions.Assertions.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.BDDMockito.given;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.gomul.rockpaperscissors.UserConsole;
import com.gomul.rockpaperscissors.game.shape.Shape;
import com.gomul.rockpaperscissors.game.RealPlayer;

@RunWith(MockitoJUnitRunner.class)
public class RealPlayerTest {
	@InjectMocks
	RealPlayer parser;
	@Mock
	UserConsole userConsole;

	@Test
	public void shouldParseScissors() {
		// given
		given(userConsole.readInput()).willReturn("scissors");

		// when
		Shape game = parser.getShape();

		// then
		assertThat(game).isSameAs(Shape.Scissors);
	}

	@Test
	public void shouldParseStone() {
		// given
		given(userConsole.readInput()).willReturn("stone");

		// when
		Shape game = parser.getShape();

		// then
		assertThat(game).isSameAs(Shape.Stone);
	}

	@Test
	public void shouldParsePaper() {
		// given
		given(userConsole.readInput()).willReturn("paper");

		// when
		Shape game = parser.getShape();

		// then
		assertThat(game).isSameAs(Shape.Paper);
	}

	@Test
	public void shouldThrowExceptionWhenUnrecognizableCommand() {
		// given
		given(userConsole.readInput()).willReturn("STHWRONG");

		try {
			// when
			Shape game = parser.getShape();
			fail("should fail");
		} catch (IllegalArgumentException e) {
			// then
		}
	}
}
