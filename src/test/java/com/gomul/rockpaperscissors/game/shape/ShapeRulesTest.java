package com.gomul.rockpaperscissors.game.shape;

import static org.fest.assertions.Assertions.*;
import org.junit.Test;

public class ShapeRulesTest {

	ShapeRules rules = new ShapeRules();

	@Test
	public void shouldSameBeEqual() {
		assertThat(rules.compare(Shape.Scissors, Shape.Scissors)).isEqualTo(0);
	}

	@Test
	public void shouldScissorsBeatPaper() {
		assertThat(rules.compare(Shape.Scissors, Shape.Paper)).isEqualTo(1);
	}

	@Test
	public void shouldPaperBeatStone() {
		assertThat(rules.compare(Shape.Paper, Shape.Stone)).isEqualTo(1);
	}

	@Test
	public void shouldStoneBeatScissors() {
		assertThat(rules.compare(Shape.Stone, Shape.Scissors)).isEqualTo(1);
	}
}
