package com.gomul.rockpaperscissors.game;

import java.util.Random;

import static org.fest.assertions.Assertions.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.BDDMockito.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.gomul.rockpaperscissors.UserConsole;
import com.gomul.rockpaperscissors.game.shape.Shape;

@RunWith(MockitoJUnitRunner.class)
public class ComputerTest {
	@Mock
	Random random;
	@Mock
	UserConsole userConsole;

	Computer computer;

	@Before
	public void init() {
		computer = new Computer(userConsole);
		computer.setRandom(random);
	}

	@Test
	public void shouldReturnShapeForIndex() {
		for (int i = 0; i < Shape.values().length; i++) {
			// given
			given(random.nextInt(Shape.values().length)).willReturn(i);

			// when
			Shape shape = computer.getShape();

			// then
			Shape shapeForOrdinar = Shape.values()[i];
			assertThat(shape).isSameAs(shapeForOrdinar);
		}
	}

	@Test
	public void shouldPrintResult() {
		// given
		given(random.nextInt(Shape.values().length)).willReturn(0);

		// when
		Shape shape = computer.getShape();

		// then

		verify(userConsole).print("" + Shape.values()[0]);
	}

}
