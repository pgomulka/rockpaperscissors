package com.gomul.rockpaperscissors;

import static org.junit.Assert.*;

import static org.fest.assertions.Assertions.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.BDDMockito.*;

import com.gomul.rockpaperscissors.game.Game;
import com.gomul.rockpaperscissors.game.Game;

@RunWith(MockitoJUnitRunner.class)
public class CommandLineInterfaceTest {
	@InjectMocks
	CommandLineInterface commandLineInterface = new CommandLineInterface();
	@Mock
	GameModeParser commandParser;
	@Mock
	UserConsole userConsole;

	@Test
	public void shouldParseCommandAndRunGame() {
		// given
		Game mockGame = mock(Game.class);
		given(userConsole.readInput()).willReturn("pvc");
		given(commandParser.parse(userConsole)).willReturn(mockGame);

		// when
		commandLineInterface.run(userConsole);

		// then
		verify(mockGame).play();
	}

}
