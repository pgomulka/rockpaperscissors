package com.gomul.rockpaperscissors.game.shape;


public enum Shape {
	Scissors, Paper, Stone;
	
	public static Shape parseShape(String name) {
		switch (name.toLowerCase()) {
		case "scissors":
			return Shape.Scissors;
		case "stone":
			return Shape.Stone;
		case "paper":
			return Shape.Paper;
		}
		throw new IllegalArgumentException();
	}
}
