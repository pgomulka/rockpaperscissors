package com.gomul.rockpaperscissors.game.shape;

import java.util.HashMap;
import java.util.Map;

public class ShapeRules {
	private Map<Shape, Shape> shapes = new HashMap<>();

	public ShapeRules() {
		shapes.put(Shape.Scissors, Shape.Paper);
		shapes.put(Shape.Paper, Shape.Stone);
		shapes.put(Shape.Stone, Shape.Scissors);
	}

	public int compare(Shape first, Shape second) {
		if (first == second)
			return 0;
		Shape beatable = shapes.get(first);
		if (beatable == second)
			return 1;
		return -1;
	}

}
