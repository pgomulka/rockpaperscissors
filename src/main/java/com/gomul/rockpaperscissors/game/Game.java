package com.gomul.rockpaperscissors.game;

import com.gomul.rockpaperscissors.UserConsole;
import com.gomul.rockpaperscissors.game.shape.Shape;

public class Game {
	private final UserConsole userConsole;
	private final int rounds = 3;
	private Player player1;
	private Player player2;

	public Game(Player player1, Player player2, UserConsole userConsole) {
		this.userConsole = userConsole;
		this.player1 = player1;
		this.player2 = player2;
	}

	public void play() {
		GameManager gameManager = new GameManager(userConsole);
		playRounds(userConsole, gameManager);

		gameManager.printScore();
	}

	private void playRounds(UserConsole userConsole, GameManager gameManager) {
		for (int i = 0; i < rounds; i++) {
			Shape userShape = player1.getShape();
			Shape computersShape = player2.getShape();
			gameManager.score(userShape, computersShape);
		}
	}

}
