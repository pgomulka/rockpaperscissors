package com.gomul.rockpaperscissors.game;

import com.gomul.rockpaperscissors.UserConsole;
import com.gomul.rockpaperscissors.game.shape.Shape;
import com.gomul.rockpaperscissors.game.shape.ShapeRules;

public class GameManager {
	private ShapeRules shapeRules = new ShapeRules();
	private int player1Score = 0, player2Score = 0;
	private final UserConsole userConsole;

	public GameManager(UserConsole userConsole) {
		this.userConsole = userConsole;
	}

	public void score(Shape player1Shape, Shape player2Shape) {
		int compare = shapeRules.compare(player1Shape, player2Shape);
		if (compare == 1)
			player1Score++;
		else if (compare == -1)
			player2Score++;
	//	userConsole.print("player2Shape: "+player2Shape);
		
	}

	public void printScore() {
		userConsole.print(player1Score + " " + player2Score);
	}
}
