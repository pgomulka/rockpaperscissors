package com.gomul.rockpaperscissors.game;

import java.util.Date;
import java.util.Random;

import com.gomul.rockpaperscissors.UserConsole;
import com.gomul.rockpaperscissors.game.shape.Shape;

public class Computer implements Player {

	private Random random = new Random(System.nanoTime());
	private final UserConsole userConsole;

	public Computer(UserConsole userConsole) {
		this.userConsole = userConsole;
	}

	public Shape getShape() {
		int ordinal = random.nextInt(Shape.values().length);
		Shape shape = Shape.values()[ordinal];
		userConsole.print(shape.toString());
		return shape;
	}

	public void setRandom(Random random) {
		this.random = random;
	}

}
