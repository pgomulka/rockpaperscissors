package com.gomul.rockpaperscissors.game;

import com.gomul.rockpaperscissors.game.shape.Shape;

public interface Player {
	Shape getShape();
}
