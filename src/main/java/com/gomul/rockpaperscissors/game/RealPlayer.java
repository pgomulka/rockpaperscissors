package com.gomul.rockpaperscissors.game;

import com.gomul.rockpaperscissors.UserConsole;
import com.gomul.rockpaperscissors.game.shape.Shape;

public class RealPlayer implements Player {
	private UserConsole userConsole;

	public RealPlayer(UserConsole userConsole) {
		this.userConsole = userConsole;
	}

	public Shape getShape() {
		String command = userConsole.readInput();
		return Shape.parseShape(command);
	}

}
