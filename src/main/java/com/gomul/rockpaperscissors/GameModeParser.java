package com.gomul.rockpaperscissors;

import com.gomul.rockpaperscissors.game.Computer;
import com.gomul.rockpaperscissors.game.Game;
import com.gomul.rockpaperscissors.game.Game;
import com.gomul.rockpaperscissors.game.RealPlayer;

public class GameModeParser {

	public Game parse(UserConsole userConsole) {
		String command = userConsole.readInput();

		if (command.equals("pvc"))
			return new Game(new RealPlayer(userConsole), new Computer(userConsole), userConsole);
		if (command.equals("cvc"))
			return new Game(new Computer(userConsole), new Computer(userConsole), userConsole);
		throw new IllegalArgumentException();
	}
}
