package com.gomul.rockpaperscissors;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class UserConsole {

	public String readInput() {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		try {
			return reader.readLine();
		} catch (IOException e) {
			e.printStackTrace();
			return "CONSOLE EXCEPTION";
		}
	}

	public void print(String message) {
		System.out.println(message);
	}

}
