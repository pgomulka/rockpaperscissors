package com.gomul.rockpaperscissors;

import com.gomul.rockpaperscissors.game.Game;

public class CommandLineInterface {

	private GameModeParser gameModeParser = new GameModeParser();

	public void run(UserConsole userConsole) {
		Game game = gameModeParser.parse(userConsole);
		game.play();
	}

	public static void main(String[] args) {
		UserConsole userConsole = new UserConsole();
		CommandLineInterface cli = new CommandLineInterface();
		cli.run(userConsole);
	}
}
